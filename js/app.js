
async function fetchQuote() {
  const response = await fetch('https://api.quotable.io/random?maxLength=100');
  
  if (response.ok) {
    const data = await response.json();
    const quote = document.querySelector('.blockquote-text');
    const author = document.querySelector('.blockquote-footer cite');
    quote.textContent = data.content;
    author.textContent = data.author;
  }
}

document.addEventListener("DOMContentLoaded", () => {
  fetchQuote();
});